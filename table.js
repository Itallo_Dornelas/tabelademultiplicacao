const input = document.getElementById("numeroMultiplicado");
const button = document.getElementById("button");
button.addEventListener("click", function () {
  const tableInput = document.getElementById("geral");
  tableInput.style.display = "block";
  button.style.display = "none";
  input.style.display = "none";
  numeroQueMultiplica();
});

function numeroQueMultiplica() {
  const inputNumero = document.querySelector("#numeroMultiplicado").value;
  criandoMultiplicacao(inputNumero);
}

function incrementoTabela(i) {
  const criandoTabelas = document.createElement("div");
  const tabelas = document.getElementById("geral");
  criandoTabelas.id = "cell";
  criandoTabelas.style.width = `100px`;
  criandoTabelas.innerHTML = i;
  tabelas.appendChild(criandoTabelas);
}

function criandoMultiplicacao(n) {
  let z = [];
  let total = [];

  for (let y = 0; y <= n; y++) {
    z[y] = [];
    for (let x = 0; x <= n; x++) {
      total = x * y;
      z[y][x] = total;
      incrementoTabela(y + "x" + x + "=" + total + "<br>");
    }
  }
}
const restart = document.getElementById("restart");
restart.addEventListener("click", function () {
  location.reload();
  button.style.display = "block";
  input.style.display = "block";
});

function showConsoleTable(n) {
  let z = [];
  let total = [];

  for (let y = 0; y <= n; y++) {
    z[y] = [];
    for (let x = 0; x <= n; x++) {
      total = x * y;
      z[y][x] = total;
    }
  }

  console.table(z);
}
showConsoleTable(10);
